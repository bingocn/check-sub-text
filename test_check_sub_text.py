"""
    Test the check_sub_text.
"""

import check_sub_text

def test_check_sub_text():
    txt_for_search = "Peter told me that peter the pickle piper piped a pitted pickle before he petered out. Phew!"
    match_dict = {
        "Peter" : [1, 20, 75],
        "peter" : [1, 20, 75],
        "pick"  : [30, 58],
        "pi"    : [30, 37, 43, 51, 58],
        "z"     : [],
        "Peterz": [],
        ""      : []
    }
    for key in match_dict.keys():
        rv = check_sub_text.check_sub_text(txt_for_search, key)
        assert set(rv)== set(match_dict[key])

    txt_for_search = "aBcDe"
    match_dict = {
        "AbC"   : [1],
        "E"     : [5],
        "abcdef": [],
        ""      : []
    }
    for key in match_dict.keys():
        rv = check_sub_text.check_sub_text(txt_for_search, key)
        assert set(rv)== set(match_dict[key])

    txt_for_search = ""
    match_dict = {
        "AbC"   : [],
        ""      : []
    }
    for key in match_dict.keys():
        rv = check_sub_text.check_sub_text(txt_for_search, key)
        assert set(rv)== set(match_dict[key])