def check_sub_text(txt_for_search, txt_for_match):
    """ Check sub text in a text. 
    Multiple matches are possible.
    Matching is case insensitive.

    Args:
        txt_for_search: The text will be searched.
        txt_for_match: The sub text will be checked.

    Returns:
        A list of the beginning position of each match for the txt_for_match within the txt_for_search.
        The beginning position starts from 1.
        If no matches, return empty list.
    """
    ret = []
    if (len(txt_for_match) < 1 or len(txt_for_search) < len(txt_for_match)): return ret
    _to_lower = lambda x: chr(ord(x) + ord('a') - ord('A')) \
                if (ord(x) >= ord('A') and ord(x) <= ord('Z')) else x
    for i in range(len(txt_for_search) - len(txt_for_match) + 1):
        for j in range(len(txt_for_match)):
            if _to_lower(txt_for_search[i+j]) != _to_lower(txt_for_match[j]): break
        else:
            ret.append(i + 1)
    return ret

def main():
    """ User could enter the txt for search and the txt for match, then check the matches and output them.
    """
    _txt_for_search = raw_input('Enter the txt for search: ')
    if len(_txt_for_search) == 0: return 0

    while True:
        _txt_for_match = raw_input('Enter the txt for match: ')
        if len(_txt_for_match) == 0: return 0
        print ','.join(str(x) for x in check_sub_text(_txt_for_search, _txt_for_match))

if __name__ == "__main__":
    main()

