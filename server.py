# -*- coding: utf-8 -*-
"""
    Start a simple web server by Flask
"""

import check_sub_text
from flask import Flask, request, redirect, render_template

app = Flask(__name__)

results = []

@app.route('/', methods=['GET', 'POST'])
def show_index():
    if request.method == 'POST':
        if len(request.form['txtforsearch']) < 1 or len(request.form['subtext']) < 1:
            render_template('index.html', entries=results)
        rv = check_sub_text.check_sub_text(request.form['txtforsearch'], request.form['subtext'])
        results.insert(0, {
            'text': request.form['txtforsearch'], 
            'subtext': request.form['subtext'],
            'matches': ','.join(str(x) for x in rv)
            })
    return render_template('index.html', entries=results)
